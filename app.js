var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var uniqueValidator = require('mongoose-unique-validator');
var jwt = require('jsonwebtoken');
var multer = require('multer');
var path = require('path');


// connect to a database
// mongodb://<user>:<password>@<host>/<database>
mongoose.connect('mongodb://localhost/Mynewdatabase');{ useNewUrlParser: true }
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){});

// use express-sessions for tracking logins
app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.text());

// serve static files from template
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/assets'));
app.use(express.static(__dirname + '/images'));
app.use(express.static(__dirname + '/css'));
app.use(express.static(__dirname + '/client'));



// include routes
var routes = require('./routes/router');
app.use('/', routes);

//include images


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// error handler, define as the last app.use callback
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});

app.post('/voice', function(req, res) {
  console.log("Received: " + req.body)
  getResponse(req.body.message, function(response){
    console.log(response)
    res.json({
      message: response
    })
  })
})

function getResponse(message, callback){
  if (message == "hello world"){
    callback("hello there. world here!")
  }
  else if (message == "goodbye")
    callback("bye-bye")
  else
    callback(message)
}






// listen on port 3000
app.listen(3000, function () {
  console.log('Listening on port 3000...');
});



