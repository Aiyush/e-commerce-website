var express = require('express');
var router = express.Router();
var User = require('../models/user');
const swal = require('sweetalert2');
var alert = require('alert-node');
var JSAlert = require("js-alert");
var uniqueValidator = require('mongoose-unique-validator');
var jwt = require('jsonwebtoken');




// GET route for homepage
router.get('/', function (req, res, next) {
  return res.sendFile(path.join(__dirname + '/views/index.html'));
});

//POST route for updating data
router.post('/', function (req, res, next) {

  

  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {

    var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
      passwordConf: req.body.passwordConf,
 
 }
 
  User.findOne({ email: req.body.email })
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } 
      else if (user){
        var err = new Error('email already exits');
        err.status = 401;
        return callback(err);
      }
  
  });
  
      
      
     
  User.create(userData, function (error, user) {
      if (error) {
        return next(error);
      } else {
        req.session.userId = user._id;
      }
    });
  

  } 

  

  else if (req.body.logemail && req.body.logpassword) {
    User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
      if (error || !user) {
        // var err = new Error('Wrong email or password!');
        // err.status = 401;
        // res(err);
    alert("Incorrect email or password");

      // JSAlert.alert("This is an alert","file saved","got it");
      } else {
        req.session.userId = user._id;
        return res.redirect('e-commerce.html');
      }
    });
  } else {
    var err = new Error('All fields are required!');
    err.status = 400;
    return next(err);
  }
})




module.exports = router;




// var express = require('express');
// var passport = require('passport');

// var router = express.Router();

// var User = require('./models/user');

// var router = express.Router();

// router.use(function (req, res, next) {
//     res.locals.currentUser = req.user;
//     res.locals.errors = req.flash('error');
//     res.locals.info = req.flash('info');
//     next();
// });

// router.get('/', function (req, res, next) {
//     User.find()
//         .sort({
//             createdAt: 'descending'
//         })
//         .exec(function (err, users) {
//             if (err) {
//                 return next(err);
//             }
//             res.render('index', {
//                 users: users
//             });
//         });
// });

// router.get('/signup', function (req, res) {
//     res.render('signup');
// })

// router.post('/signup', function (req, res, next) {
//     var username = req.body.user;
//     var password = req.body.pass;

//     User.findOne({
//         username: username
//     }, function (err, user) {
//         if (err) {
//             return next(err);
//         }
//         if (user) {
//             req.flash('error', 'User already exists');
//             return res.redirect('/signup');
//         }

//         var newUser = new User({
//             username: username,
//             password: password
//         });
//         newUser.save(next);

//     });
// }, passport.authenticate("login", {
//     successRedirect: "/",
//     failureRedirect: "/signup",
//     failureFlash: true
// }));

// router.get("/users/:username", function (req, res, next) {
//     User.findOne({
//         username: req.params.username
//     }, function (err, user) {
//         if (err) {
//             return next(err);
//         }
//         if (!user) {
//             return res.status(404).send('404: File not found!');
//         }
//         res.render("profile", {
//             user: user
//         });
//     });
// });

// router.get('/login', function (req, res) {
//     res.render('login')
// });

// router.post('/login', passport.authenticate('login', {
//     successRedirect: "/",
//     failureRedirect: "/signup",
//     failureFlash: true
// }));

// router.get("/logout", function (req, res) {
//     req.logout();
//     res.redirect("/");
// });

// router.use(function (req, res, next) {
//     res.locals.currentUser = req.user;
//     res.locals.errors = req.flash("error");
//     res.locals.infos = req.flash("info");
//     next();
// });

// router.get("/edit", ensureAuthenticated, function (req, res) {
//     res.render("edit");
// });

// router.post("/edit", ensureAuthenticated, function (req, res, next) {
//     req.user.displayName = req.body.displayname;
//     req.user.bio = req.body.bio;
//     req.user.save(function (err) {
//         if (err) {
//             next(err);
//             return;
//         }
//         req.flash("info", "Profile updated!");
//         res.redirect("/edit");
//     });
// });

// function ensureAuthenticated(req, res, next) {
//     if (req.isAuthenticated()) {
//         next();
//     } else {
//         req.flash("info", "You must be logged in to see this page.");
//         res.redirect("/login");
//     }
// }

// module.exports = router;